EESchema Schematic File Version 2  date Thu 28 Feb 2013 10:26:32 PM MST
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:beaglebone
LIBS:gadgets
LIBS:brewery-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date "1 mar 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SCR U?
U 1 1 512FB5EF
P 3550 2000
F 0 "U?" H 3700 2250 70  0000 C CNN
F 1 "SCR" H 3700 1650 70  0000 C CNN
F 2 "~" H 3550 2000 60  0000 C CNN
F 3 "~" H 3550 2000 60  0000 C CNN
	1    3550 2000
	1    0    0    -1  
$EndComp
$Comp
L SCR U?
U 1 1 512FB5FE
P 4350 2000
F 0 "U?" H 4500 2250 70  0000 C CNN
F 1 "SCR" H 4500 1650 70  0000 C CNN
F 2 "~" H 4350 2000 60  0000 C CNN
F 3 "~" H 4350 2000 60  0000 C CNN
	1    4350 2000
	1    0    0    -1  
$EndComp
$Comp
L SCR U?
U 1 1 512FB60D
P 5250 2000
F 0 "U?" H 5400 2250 70  0000 C CNN
F 1 "SCR" H 5400 1650 70  0000 C CNN
F 2 "~" H 5250 2000 60  0000 C CNN
F 3 "~" H 5250 2000 60  0000 C CNN
	1    5250 2000
	1    0    0    -1  
$EndComp
$Comp
L SCR U?
U 1 1 512FB61C
P 6200 2000
F 0 "U?" H 6350 2250 70  0000 C CNN
F 1 "SCR" H 6350 1650 70  0000 C CNN
F 2 "~" H 6200 2000 60  0000 C CNN
F 3 "~" H 6200 2000 60  0000 C CNN
	1    6200 2000
	1    0    0    -1  
$EndComp
$Comp
L SHIFT_REGISTER U?
U 1 1 513036C9
P 4950 3800
F 0 "U?" H 4950 3900 60  0000 C CNN
F 1 "SHIFT_REGISTER" H 4950 3800 60  0000 C CNN
F 2 "" H 4950 3800 60  0000 C CNN
F 3 "" H 4950 3800 60  0000 C CNN
	1    4950 3800
	1    0    0    -1  
$EndComp
Text HLabel 9200 2850 0    60   UnSpc ~ 0
GND
Wire Wire Line
	4400 3950 2650 3950
Wire Wire Line
	2650 3950 2650 2400
Wire Wire Line
	2650 2400 3200 2400
Wire Wire Line
	4400 4050 3850 4050
Wire Wire Line
	3850 4050 3850 2400
Wire Wire Line
	3850 2400 4000 2400
Wire Wire Line
	4400 4150 3950 4150
Wire Wire Line
	3950 4150 3950 2700
Wire Wire Line
	3950 2700 4800 2700
Wire Wire Line
	4800 2700 4800 2400
Wire Wire Line
	4800 2400 4900 2400
Wire Wire Line
	4400 4250 4050 4250
Wire Wire Line
	4050 4250 4050 2800
Wire Wire Line
	4050 2800 5700 2800
Wire Wire Line
	5700 2800 5700 2400
Wire Wire Line
	5700 2400 5850 2400
Wire Wire Line
	9200 2850 9200 3150
Wire Wire Line
	9200 3150 5850 3150
Wire Wire Line
	5850 3150 5850 4900
Wire Wire Line
	5850 4900 4250 4900
Wire Wire Line
	4250 4900 4250 4650
Wire Wire Line
	4250 4650 4400 4650
$EndSCHEMATC
